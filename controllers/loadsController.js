const User = require("../models/userModel");
const Truck = require("../models/truckModel");
const Load = require("../models/loadModel");
const Mongoose = require("mongoose");
const ObjectId = Mongoose.Types.ObjectId;
const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];
const status = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const state = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];

class loadsController {
    async addLoad(req, res) {
        console.log(req.body);
        try {
            const load = await new Load({
                created_by: new ObjectId(req.user._id),
                name: req.body.name,
                payload: +req.body.payload,
                pickup_address: req.body.pickup_address,
                delivery_address: req.body.delivery_address,
                dimensions: req.body.dimensions
            });
            await load.save();
            console.log(load);
            res.status(200).json({
                message: 'Load created successfully',
                load: load
            });

        } catch(err) {
            res.status(500).json({
                message: "Internal server error"
            });
        }
    }
    async deleteLoad(req, res) {
        try {
            const id = req.params.id;
            console.log(req.params.id);
            if(!id) {
                res.status(400).json({ message: "Wrong id"});
            }
            const load = await Load.deleteOne({_id: id});
            if(load.deletedCount === 0) {
                res.status(400).json({ message: "No load with this id"});
            }
            res.status(200).json({ message: "Load deleted successfully"});
        } catch(e) {
            res.status(500).json({message: "Internal server error"})
        }
    }

}

module.exports = new loadsController();
const User = require('../models/userModel');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {validationResult} = require('express-validator');
const secret = process.env.SECRET;

const generateAccessToken = (id, email) => {
    const payload = {
        id,
        email
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"})
}

class authController {
    async registration(req, res) {
        try {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(400).json({message: "Registration error"})
            }
            const {email, password, role} = req.body;
            const candidate = await User.findOne({email});
            if(candidate) {
                return res.status(400).json({message: "User with this name has already existed"})
            }
            const hashPassword = bcrypt.hashSync(password, 7);
            const user = new User({email, password: hashPassword, role});
            await user.save();
            return res.status(200).json({message: "Profile created successfully"})
        } catch(e) {
            console.log(e);
            res.status(500).json({message: "Internal server error"})
        }
    }
    async login(req, res) {
        try {
            const {email, password} = req.body;
            const user = await User.findOne({email})
            if(!user) {
                return res.status(400).json({message: `User ${email} not found`});
            }
            const validPassword = bcrypt.compareSync(password, user.password);
            if(!validPassword) {
                return res.status(400).json({message: "Incorrect password"});
            }
            const token = generateAccessToken(user._id, user.email);
            return res.status(200).json({"jwt_token": token})
        } catch(e) {
            res.status(500).json({message: "Internal server error"})
        }
    }
}

module.exports = new authController();
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();
const morgan = require('morgan');
// const authRouter = require('./routes/authRouter')
const router = require('./routes/allRoutes');
const cors = require('cors');

const app = express();
app.use(morgan('tiny'));
app.use(express.json());
app.use(cors());
app.use(router);

const portNumber = 8080;
const PORT = +process.env.PORT || portNumber;
const database = process.env.DB;

const start = async () => {
  try {
    await mongoose.connect(database, {useNewUrlParser: true});
    app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
  } catch (e) {
    console.log(e);
  }
};
start();

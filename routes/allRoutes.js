const express = require('express');
const authMiddleware = require('../middleware/authMiddleware');
const driverMiddleware = require('../middleware/driverMiddleware');
const shipperMiddleware = require('../middleware/shipperMiddleware');
const authRouter = require('./authRouter');
const usersRouter = require('./usersRouter');
const trucksRouter = require('./trucksRouter');
const loadsRouter = require('./loadsRouter');

const router = new express.Router();

router.use('/api/auth', authRouter);
router.use('/api/users/me', authMiddleware, usersRouter);
router.use('/api/trucks', authMiddleware, driverMiddleware, trucksRouter);
router.use('/api/loads', authMiddleware,  shipperMiddleware, loadsRouter);

module.exports = router;

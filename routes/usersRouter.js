const Router = require('express');
const router = new Router();
const usersController = require('../controllers/usersController');
const {check} = require('express-validator');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/', authMiddleware, usersController.getProfileInfo);
router.delete('/', authMiddleware, usersController.deleteProfile);
router.patch('/password', authMiddleware, usersController.changePassword);

module.exports = router

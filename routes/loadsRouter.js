const Router = require('express');
const router = new Router();
const loadsController = require('../controllers/loadsController');

router.post('/', loadsController.addLoad);
router.delete('/:id', loadsController.deleteLoad);
// router.get('/trucks/:id', trucksController.getUsersTruckById);
// router.put('/trucks/:id', trucksController.updateUsersTruckById);
// router.delete('/trucks/:id', trucksController.deleteUsersTruckById);
// router.post('/trucks/:id/assign', trucksController.assignTruck);

module.exports = router
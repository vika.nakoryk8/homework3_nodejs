const User = require("../models/userModel");

const driverMiddleware = async (req, res, next) => {
    const user = await User.findById({_id: req.user.id});
    if(!user) {
        res.status(400).json({message: "Incorrect user"});
        throw "Incorrect user";
    }
    if (user.role !== "DRIVER") {
        res.status(400).json({message: "You are not a driver"});
        throw "You are not a driver";
    }
    next();

};

module.exports = driverMiddleware;

const User = require("../models/userModel");

const shipperMiddleware = async (req, res, next) => {
    const user = await User.findById({_id: req.user.id});
    if(!user) {
        res.status(400).json({message: "Incorrect user"});
        throw "Incorrect user";
    }
    if (user.role !== "SHIPPER") {
        res.status(400).json({message: "You are not a shipper"});
        throw "You are not a shipper";
    }
    next();

};

module.exports = shipperMiddleware;

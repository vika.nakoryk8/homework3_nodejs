const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loadSchema = new Schema({
    created_by: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    assigned_to: {
        type: mongoose.Types.ObjectId,
        default: null
    },
    status: {
        type: String,
        default: "NEW",
        required: true
    },
    state: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    payload: {
        type: Number
    },
    pickup_address: {
        type: String,
        required: true
    },
    delivery_address: {
        type: String,
        required: true
    },
    dimensions: {
        width: Number,
        length: Number,
        height: Number
    },
    logs: {
        type: [{
            message: {
                type: String,
                required: true
            },
            time: {
                type: Date,
                default: Date.now()
            }
        }]
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
}, {timestamps: {
        createdAt: false,
        updatedAt: false,
    },
    versionKey: false})

const Load = mongoose.model('Load', loadSchema);
module.exports = Load
